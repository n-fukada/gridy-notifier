import moment from 'moment';
import { writeFile, readFileSync } from "fs";

const CONFIG_FILE_PATH: string = 'config.json';

export class Config {
  private _lastAccess: moment.Moment = moment(0);

  get lastAccess(): moment.Moment {
    return this._lastAccess;
  }

  load() {
    try {
      let dataStr = readFileSync(CONFIG_FILE_PATH).toString();
      let data = JSON.parse(dataStr);
      if (data.lastAccess) {
        this._lastAccess = moment(data.lastAccess);
      }
    } catch (err) {
      console.log(`failed to load config: ${err}`);
    }
  }

  save() {
    const data = {
      lastAccess: this._lastAccess,
    };
    writeFile(CONFIG_FILE_PATH, JSON.stringify(data), (err) => {
      if (err) {
        console.log(`failed to save config: ${err}`);
      }
    });
  }

  updateAccess() {
    this._lastAccess = moment();
  }
}
