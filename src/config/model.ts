export interface Environment {
  login: {
    url: string,
    form: string,
    submit: string,
    fields: {}
  };
  pages: NotifyPage[];
  timeOffset: string;
  slack: {
    url: string,
  };
}

export interface NotifyPage {
  url: string;
  pretext: string;
  selector: string;
  time: TimeField;
  fields: NotifyField[];
  color?: string;
  icon_emoji?: string;
}

export interface InputField {
  selector: string;
  value: string;
}

export interface NotifyField {
  selector: string;
  eq?: number;
  property?: string;
  regexp?: {
    pattern: RegExp,
    replace: string,
  };
  ignore?: RegExp;
}

export interface TimeField extends NotifyField {
  format: string;
}
