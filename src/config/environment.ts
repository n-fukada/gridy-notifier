import dotenv from "dotenv";
import moment from "moment";
import { Environment } from './model';

dotenv.config();

export const env: Environment = {
  login: {
    url: 'https://gridy.net/login',
    form: '#login_form',
    submit: 'input[id=submit]',
    fields: {
      uname: process.env.LOGIN_USERNAME,
      upass: process.env.LOGIN_PASSWORD
    }
  },
  pages: [{
    url: 'https://gridy.net/message',
    pretext: "新着メッセージがあります",
    icon_emoji: ":email:",
    selector: '#result_data tr:not(.hidden_item)',
    time: {
      selector: "td",
      eq: 1,
      format: "YYYY/MM/DD HH:mm",
    },
    fields: [
      {
        selector: "td a",
        regexp: {
          pattern: /^(.*)$/,
          replace: "*$1*",
        }
      },
      {
        selector: "td",
        eq: 3,
        regexp: {
          pattern: /^\s+(\S.+\S)\s+$/,
          replace: "$1",
        },
        ignore: new RegExp(process.env.YOUR_NAME)
      }
    ]
  },
  {
    url: 'https://gridy.net/file/search',
    pretext: "新着ファイルがあります",
    icon_emoji: ":paperclip:",
    selector: ".list_table tbody tr",
    color: "#3AA3E3",
    time: {
      selector: "td",
      eq: 5,
      format: "YYYY/MM/DD HH:mm",
    },
    fields: [{
      selector: "td span a",
      eq: 0,
      regexp: {
        pattern: /^(.*)$/,
        replace: "*$1*",
      },
    }, {
      selector: "td",
      eq: 6,
      ignore: new RegExp(process.env.YOUR_NAME)
    }],
  },
  {
    url: 'https://gridy.net/memopad/index',
    pretext: "新着メモがあります",
    icon_emoji: ":memo:",
    selector: "#mail_index tbody tr",
    color: "#fa8072",
    time: {
      selector: "td",
      eq: 6,
      format: "YYYY/MM/DD HH:mm",
    },
    fields: [{
      selector: "td a",
      regexp: {
        pattern: /^(.*)$/,
        replace: "*$1*",
      },
    }, {
      selector: "td",
      eq: 3
    }],
  }],
  timeOffset: "+0900",
  slack: {
    url: process.env.SLACK_URL
  },
};

export function getLocalTime(time: string, timeFormat: string) {
  if (!moment(time, timeFormat, true).isValid()) {
    return null;
  }
  return moment(`${time} ${env.timeOffset}`, `${timeFormat} Z`).utc();
}
