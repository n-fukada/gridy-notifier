
import cheerio from 'cheerio-httpcli';
import moment from 'moment';
import { NotifyPage, NotifyField } from './config/model';
import { env } from './config/environment';
import { Config } from './config/configfile';
import { SlackService } from './slack';

export interface NotifyItem {
  time: string,
  fields: string[]
}

export class HttpService {
  private config: Config;
  private slack: SlackService;

  constructor(config: Config) {
    this.config = config;
    this.slack = new SlackService();
  }

  /**
   * Environmentで設定したページ全てにアクセス
   */
  async access() {
    console.log({
      now: moment(),
      lastAccess: this.config.lastAccess
    });
    for (const page of env.pages) {
      await this.accessPage(page);
    }
  }

  /**
   * 指定ページにアクセス
   * @param {NotifyPage} page 取得ページ設定
   * @returns {Promise<void>}
   */
  private async accessPage(page: NotifyPage): Promise<void> {
    try {
      let result = await cheerio.fetch(page.url);
      // セレクタから要素を取得できない場合、ログイン処理
      if (result.$(page.selector).length <= 0) {
        result = await this.login(result);
      }
      let items = await this.fetchItems(page);
      items = this.updateItems(items, page);
      if (items.length > 0) {
        console.log(items);
        this.slack.sendMessage(items, page);
      } else {
        console.log('no update items');
      }
    } catch (err) {
      console.log(err);
    }
  }

  /**
   * ログイン
   * @param {cheerio.FetchResult} result
   * @returns {cheerio.Promise}
   */
  private login(result: cheerio.FetchResult): cheerio.Promise {
    let form = result.$(env.login.form);
    form.field(env.login.fields);
    return form.find(env.login.submit).click();
  }

  /**
   * 更新監視対象の要素を取得する
   * @param {NotifyPage} page 取得ページ設定
   * @returns {Promise<NotifyItem[]>} 更新監視対象要素
   */
  private async fetchItems(page: NotifyPage): Promise<NotifyItem[]> {
    // access
    let result = await cheerio.fetch(page.url);
    let items: NotifyItem[] = [];
    result.$(page.selector).each((index) => {
      const elem = result.$(page.selector).eq(index);
      // get time
      const time: string = this.elemText(elem, page.time);
      // get fields
      let fields: string[] = [];
      page.fields.forEach((field) => {
        fields.push(this.elemText(elem, field));
      })
      items.push({time, fields});
    });
    return items;
  }

  /**
   * 指定要素下のtextを取得
   * @param {Cheerio} $
   * @param {NotifyField} field
   * @returns {string}
   */
  private elemText($: Cheerio, field: NotifyField): string {
    let elem = $.find(field.selector);
    // eqがあれば、eq番目の要素を取得
    elem = field.eq ? elem.eq(field.eq) : elem;
    let text = elem.text();
    return field.regexp ? text.replace(field.regexp.pattern, field.regexp.replace) : text;
  }

  /**
   * 監視対象要素から更新要素のみ取得
   * @param {NotifyItem[]} items 監視対象要素
   * @param {NotifyPage} page 取得ページ設定
   * @returns {NotifyItem[]} 更新要素List
   */
  private updateItems(items: NotifyItem[], page: NotifyPage): NotifyItem[] {
    return items.filter((item) => {
      const time = moment(`${item.time} ${env.timeOffset}`, `${page.time.format} Z`);
      const isNew = time.isSameOrAfter(this.config.lastAccess);
      if (!isNew) {
        return isNew;
      }
      for (let i = 0; i < page.fields.length; i++) {
        // ignoreなフィールドがある場合は更新チェック対象外
        if (page.fields[i].ignore && item.fields[i].match(page.fields[i].ignore)) {
          return false;
        }
      }
      return isNew;
    });
  }
}
