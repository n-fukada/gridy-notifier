import request from 'request';
import { NotifyPage } from './config/model';
import { env, getLocalTime } from './config/environment';
import { NotifyItem } from './http';

export class SlackService {
  sendMessage(items: NotifyItem[], page: NotifyPage) {
    console.log(`send message: ${items.length} item${items.length > 1 ? 's' : ''}`);
    let attachments = [];
    items.forEach((item) => {
      let fields = [];
      item.fields.forEach((value) => {
        const field = { value, short: true };
        fields.push(field);
      });
      attachments.push({
        color: page.color || 'good',
        fields: fields,
        mrkdwn_in: ['fields', 'pretext'],
        ts: getLocalTime(item.time, page.time.format).unix().toString()
      });
    })
    attachments[0].pretext = page.pretext;
    const options = {
      uri: env.slack.url,
      headers: {
        "Content-type": "application/json",
      },
      json: {
        attachments,
        icon_emoji: page.icon_emoji
      }
    };
    request.post(options, function(error, response, body){
      if (response && response.statusCode != 200) {
        console.error(`failed to send message: ${response.statusCode} ${body ? body: ""}`);
      }
    });
  }
}
