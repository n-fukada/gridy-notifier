import { Config } from './config/configfile';
import { HttpService } from './http';

main();

async function main() {

  const config = new Config();
  config.load();

  const http = new HttpService(config);
  await http.access();

  config.updateAccess();
  config.save();
}
