# gridy-notifier

<img width="497" alt="gridy_notifier_sample" src="https://bitbucket.org/repo/XXn8qrb/images/832586386-gridy-notifier.png">

`gridy-notifier` はグループウェアGRIDYの更新チェック・通知を行うスクリプトです。

GRIDYにログインし、メッセージ・ファイル・メモの一覧を取得し、更新があった場合はSlackに通知します。

TypeScriptで書かれており、Node.jsで動作します。


## 必要物

Node.js


## セットアップ

1. ソースコードをダウンロード、トランスパイルします。

   ```bash
   $ git clone git://github.com/n-fukada/gridy-notifier.git
   $ cd gridy-notifier
   $ npm install
   $ npm run build
   ```

2. `.env.example` をリネームし、各項目を編集します。

   ```bash
   $ mv .env.example .env
   $ vi .env
   ```

   編集する項目は以下となります。

   | 項目名         | 内容                                                   |
   | -------------- | ------------------------------------------------------ |
   | LOGIN_USERNAME | ログインユーザ名（通常はメールアドレス）               |
   | LOGIN_PASSWORD | ログインパスワード                                     |
   | YOUR_NAME      | ユーザ名。通知から自分の投稿を除外するために使用します |
   | SLACK_URL      | SlackのIncoming Webhook URL                            |


## 使い方

```bash
$ npm start
```

スクリプトを1度実行します。

定期的に自動実行するには、cron等の設定が必要となります。


### crontab設定例

以下は9時から18時まで1時間おきにスクリプトを実行する例です。

```
0 9-18 * * * npm start --prefix "スクリプト配置先ディレクトリ絶対パス"
```

スクリプトの実行間隔にはくれぐれもご注意ください。
